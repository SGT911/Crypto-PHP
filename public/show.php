<?php
	require '../utils.php';

	$http_code = 200;
	if ($_SERVER['REQUEST_METHOD'] != 'POST') {
		$http_code = 405;
	} else if (!is_numeric($_POST['key1']) || !is_numeric($_POST['key2'])) {
		$return_url = "/?msg=" . urlencode($_POST['msg']);
		$http_code = 400;
	} else {
		$data = array(
			'message' => normalize($_POST['msg']),
			'key_1' => (int) $_POST['key1'],
			'key_2' => (int) $_POST['key2'],
			'action' => strtolower($_POST['action']),
		);

		$res_data = json_decode(api_request(
			"https://msg-encrypt.herokuapp.com/api/" . $data['action'] . "/" . $data['key_1'] . "." . $data['key_2'],
			'POST',
			array('message' => $data['message'])
		));

		$title = ($data['action'] == 'encrypt')? 'Mensaje Encriptado' : 'Mensaje Desencriptado';
	}

	if ($http_code != 200) {
		http_response_code($http_code);
	}
?>

<html lang="es">
	<head>
		<?php include '../helpers/meta.php'; ?>
	</head>
	<body>
		<?php if ($http_code != 200): ?>
			<?php include "../helpers/errors/$http_code.php"; ?>
		<?php else: ?>
			<div class="card spacer" style="--space: 3em">
				<div class="card-content">
					<p class="title">Resultado del Mensaje</p>
					<hr />
					<?php if ($data['action'] == 'encrypt'): ?>
						<p><b>Mensaje original:</b> "<?= $data['message'] ?>"</p>
					<?php endif; ?>
					<div class="columns">
						<div class="column">
							<p><b>1° clave:</b> <?= $data['key_1'] ?></p>
						</div>
						<div class="column">
							<p><b>2° clave:</b> <?= $data['key_2'] ?></p>
						</div>
					</div>
					<hr>
					<p>Mensaje <?= ($data['action'] == 'encrypt')? 'encriptado' : 'desencriptado' ?></p> <br>
					<pre><?= '"' . $res_data . '"' ?></pre>
					<?php if ($data['action'] == 'encrypt'): ?>
						<?php $link = '/direct.php?msg=' . urlencode($res_data) . '&k1=' . $data['key_1'] . '&k2=' . $data['key_2']; ?>
						<hr>
						<p>Link para desencriptar</p><br>
						<a href="<?= $link ?>">
							<pre><?= 'http' . (($_SERVER['SERVER_PORT'] == 443)? 's' : '') . '://' . $_SERVER['HTTP_HOST'] . $link ?></pre>
						</a>
					<?php endif; ?>
				</div>
				<footer class="card-footer">
					<a href="/" class="card-footer-item button is-info is-light">Go home</a>
				</footer>
			</div>
		<?php endif; ?>
	</body>
</html>