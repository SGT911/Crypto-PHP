<?php
	require '../utils.php';

	$title = "Encriptar";

	$key_1 = 0;
	$key_2 = 0;

	$msg = '';
	if (isset($_GET['msg'])) {
		$msg = $_GET['msg'];
	}

	if (isset($_GET['key1']) && isset($_GET['key2'])) {
		if ((is_numeric($_GET['key1']) && is_numeric($_GET['key2']))) {
			$key_1 = (int) $_GET['key1'];
			$key_2 = (int) $_GET['key2'];
		}
	}

	if ($_POST['rand'] == 'yes') {
		$key_1 = (int) rand(6,100);
		$key_2 = (int) rand(6,100);
	}
?>
<html lang="es">
	<head>
		<?php include '../helpers/meta.php'; ?>
	</head>
	<body>
		<h1 class="title text-center spacer" style="--space: 0.5em">Encripta tus mensajes</h1>
		<form
			class="form spacer"
			method="POST"
			action="show.php"
		>
			<input type="hidden" name="rand" value="no">
			<input type="hidden" name="action" value="encrypt">
			<div class="field">
				<label for="msg" class="label">Mensaje</label>
				<div class="control">
					<input
						type="text"
						class="input"
						placeholder="Mensaje de una linea"
						name="msg"
						data-only-ascii="on"
						value="<?= $msg ?>"
					>
				</div>
			</div>
			<div class="columns">
				<div class="column field">
					<label for="key1" class="label">1° llave</label>
					<div class="control">
						<input
							type="number"
							name="key1"
							class="input"
							placeholder="Llave numerica"
							data-only-number="on"
							value="<?= $key_1 ?>"
						>
					</div>
				</div>
				<div class="column field">
					<label for="key2" class="label">2° llave</label>
					<div class="control">
						<input
							type="number"
							name="key2"
							class="input"
							placeholder="Llave numerica"
							data-only-number="on"
							value="<?= $key_2 ?>"
						>
					</div>
				</div>
			</div>
			<div class="field is-grouped">
				<div class="control">
					<button class="button is-info" id="submit">Enviar</button>
				</div>
				<div class="control">
					<a href="/decrypt.php" class="button is-link is-light is-text">Desencriptar!!</a>
				</div>
				<div class="control">
					<button
						class="button is-danger is-light"
						data-clear="msg, key1, key2"
					>Borrar todo</button>
				</div>
				<div class="control">
					<button class="button is-primary is-light" id="rand">Generar llaves</button>
				</div>
			</div>
		</form>
		<?= get_js_file('form.js') ?>
		<?= get_js_file('page/common_check_crypt_form.js') ?>
		<?= get_js_file('page/index.js') ?>
	</body>
</html>