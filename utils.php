<?php
	function get_js_file($filename, $is_module = false) {
		$module = ($is_module)? 'type="module"' : null;
		return "<script" . $module . ">" . file_get_contents('../js/' . $filename) ."</script>";
	}

	function api_request($url, $method, $data = null) {
		$options = array('method' => $method);
		if ($data != null) {
			$options['content'] = json_encode($data);
			$options['header'] = "Content-type: application/json\r\n";
		}

		$ctx = stream_context_create(array('http' => $options));
		$result = file_get_contents($url, false, $ctx);

		if ($result == false) {
			return null;
		}

		return $result;
	}

	function normalize($str) {
		$str = str_replace("'", '', $str);
		$str = str_replace('"', '', $str);
		$str = str_replace('"', '', $str);
		$str = str_replace('<', '', $str);
		$str = str_replace('>', '', $str);
		$str = str_replace('{', '', $str);
		$str = str_replace('}', '', $str);
		$str = str_replace('[', '', $str);
		$str = str_replace(']', '', $str);
		$str = str_replace('´', '', $str);
		$str = str_replace('`', '', $str);
		$str = str_replace('^', '', $str);
		$str = str_replace('\\', '', $str);
		$str = str_replace('/', '', $str);

		$str = str_replace('Á', 'A', $str);
		$str = str_replace('À', 'A', $str);
		$str = str_replace('Í', 'I', $str);
		$str = str_replace('Ì', 'I', $str);
		$str = str_replace('Ú', 'U', $str);
		$str = str_replace('Ù', 'U', $str);
		$str = str_replace('É', 'E', $str);
		$str = str_replace('È', 'E', $str);
		$str = str_replace('Ó', 'O', $str);
		$str = str_replace('Ò', 'O', $str);

		$str = str_replace('á', 'a', $str);
		$str = str_replace('à', 'a', $str);
		$str = str_replace('í', 'i', $str);
		$str = str_replace('ì', 'i', $str);
		$str = str_replace('ú', 'u', $str);
		$str = str_replace('ù', 'u', $str);
		$str = str_replace('é', 'e', $str);
		$str = str_replace('è', 'e', $str);
		$str = str_replace('ó', 'o', $str);
		$str = str_replace('ò', 'o', $str);

		return $str;
	}
?>