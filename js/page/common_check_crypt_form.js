document.getElementById('submit').onclick = function(evt) {
	const form = document.getElementsByTagName('form')[0];

	let empty = `${form.msg.value}${form.key1.value}${form.key2.value}`
	if (empty == '') {
		alert("Todos los campos deben estar llenos.")
		evt.preventDefault();
	}

	if (parseInt(form.key1.value) <= 5 || parseInt(form.key2.value) <= 5) {
		alert("Las llaves deben ser mayor que \"5\".")
		evt.preventDefault();
	}

	if (parseInt(form.key1.value) == parseInt(form.key2.value)) {
		alert("Las llaves no pueden ser iguales.")
		evt.preventDefault();
	}
}