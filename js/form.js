document.querySelectorAll('input[data-only-ascii="on"]').forEach(el => {
	el.addEventListener('input', function() {
		el.value = el.value
			// Special chars
			.replace(/\'/g, '')
			.replace(/\"/g, '')
			.replace(/\</g, '')
			.replace(/\>/g, '')
			.replace(/\{/g, '')
			.replace(/\}/g, '')
			.replace(/\[/g, '')
			.replace(/\]/g, '')
			.replace(/\´/g, '')
			.replace(/\`/g, '')
			.replace(/\^/g, '')
			.replace(/\\/g, '')
			.replace(/\//g, '')
			// Mayus with tilde
			.replace(/Á/g, 'A')
			.replace(/À/g, 'A')
			.replace(/Í/g, 'I')
			.replace(/Ì/g, 'I')
			.replace(/Ú/g, 'U')
			.replace(/Ù/g, 'U')
			.replace(/É/g, 'E')
			.replace(/È/g, 'E')
			.replace(/Ó/g, 'O')
			.replace(/Ò/g, 'O')
			// Tilde
			.replace('á', 'a')
			.replace('à', 'a')
			.replace('í', 'i')
			.replace('ì', 'i')
			.replace('ú', 'u')
			.replace('ù', 'u')
			.replace('é', 'e')
			.replace('è', 'e')
			.replace('ó', 'o')
			.replace('ò', 'o');

	})
});

document.querySelectorAll('input[data-only-number="on"]').forEach(el => {
	el.addEventListener('input', function() {
		el.value = el.value.replace(/\D/g, '');
	})
});

document.querySelectorAll('button[data-clear]').forEach(el => {
	const clearNames = el.dataset['clear'].split(',').map(str => str.trim());

	el.addEventListener('click', function(evt) {
		evt.preventDefault();

		for (let i = clearNames.length - 1; i >= 0; i--) {
			const name = clearNames[i];

			document.getElementsByName(name).forEach(el => {
				el.value = '';
			})
		}
	})
})