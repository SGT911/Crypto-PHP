<?php
	define('CSS_FILES', [
		"bulma/bulma.min.css",
		"app.css"
	]);
	define('TITLE', 'Crypto PHP');

	function get_title($title) {
		if (!is_null($title)) {
			return $title . ' | ' . TITLE;
		}

		return TITLE;
	}
?>

<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">

<title>
	<?= get_title( (isset($title))? $title : null ) ?>
</title>

<?php foreach (CSS_FILES as $file): ?>
	<!-- <?= $file ?> -->
	<style><?= file_get_contents('../css/' . $file) ?></style>
<?php endforeach; ?>