<?php
	$return_url = (isset($return_url))? $return_url : '/';
?>

<div class="card spacer" style="--space: 3em">
	<header class="card-header">
		<h1 class="title card-header-title">
			<em>405</em>&ensp;Method not allowed
		</h1>
	</header>
	<div class="card-content">
		The data received is incorrect. Chack and retry
	</div>
	<footer class="card-footer">
		<a href="<?= $return_url ?>" class="card-footer-item">Go home</a>
	</footer>
</div>